package com.example.cbk12_000.lab1;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;

public class A3 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_a3);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


    }
    public void onClickedbtn(View v){
        Intent myIntent = new Intent();
        EditText edit = (EditText)findViewById(R.id.T4);
        myIntent.putExtra("textValue", edit.getText().toString());
        setResult(Activity.RESULT_OK, myIntent);
        finish();
    }

}
