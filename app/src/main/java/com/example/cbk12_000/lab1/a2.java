package com.example.cbk12_000.lab1;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;

public class a2 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_a2);
        EditText T2 = (EditText)findViewById(R.id.T2);
        T2.setText("hello " + getIntent().getStringExtra("Text"));


    }
    public void onClickedbtn(View v){
        Intent myIntent = new Intent(this, A3.class);
        startActivityForResult(myIntent, 1);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if(resultCode == RESULT_OK) {
                EditText T3 = (EditText)findViewById(R.id.T3);
                T3.setText("From A3: " +data.getStringExtra("textValue"));
            }
        }
    }
}
