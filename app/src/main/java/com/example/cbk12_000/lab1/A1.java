package com.example.cbk12_000.lab1;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.Nullable;
import android.support.annotation.XmlRes;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.Set;

public class A1 extends AppCompatActivity {

    public ArrayList<String> list;
    Spinner S1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_a1);
        list = new ArrayList<String>();
        list.add("Option 1");
        list.add("Option 2");
        list.add("Option 3");
        list.add("Option 4");
        S1 = (Spinner)findViewById(R.id.L1);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, list);
        S1.setAdapter(adapter);
        SharedPreferences prefs = getSharedPreferences("test", MODE_PRIVATE);
        String name = prefs.getString("option", "No name defined");
        for(int i = 0; i < list.size(); i++){
            if(name.contains(list.get(i))){
                S1.setSelection(i);
            }
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        SharedPreferences.Editor editor = getSharedPreferences("test", MODE_PRIVATE).edit();
        editor.putString("option", S1.getSelectedItem().toString());
        editor.apply();
    }

    public void onClickbtn(View v) {
        Intent myIntent = new Intent(this, a2.class);
        EditText edit = (EditText)findViewById(R.id.T1);
        myIntent.putExtra("Text", edit.getText().toString());

        startActivity(myIntent);
    }
}
